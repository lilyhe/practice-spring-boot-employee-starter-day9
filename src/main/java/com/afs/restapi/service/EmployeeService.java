package com.afs.restapi.service;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.repository.InMemoryEmployeeRepository;
import com.afs.restapi.repository.JPAEmployeeRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class EmployeeService {

    private final InMemoryEmployeeRepository inMemoryEmployeeRepository;
    private final JPAEmployeeRepository jpaEmployeeRepository;

    public EmployeeService(InMemoryEmployeeRepository inMemoryEmployeeRepository, JPAEmployeeRepository jpaEmployeeRepository) {
        this.inMemoryEmployeeRepository = inMemoryEmployeeRepository;
        this.jpaEmployeeRepository = jpaEmployeeRepository;
    }

    public InMemoryEmployeeRepository getEmployeeRepository() {
        return inMemoryEmployeeRepository;
    }

    public JPAEmployeeRepository getJpaEmployeeRepository() {
        return jpaEmployeeRepository;
    }

    public List<Employee> findAll() {
        return jpaEmployeeRepository.findAll();
    }

    public Employee findById(Long id) {
        return jpaEmployeeRepository.findById(id)
                .orElseThrow(EmployeeNotFoundException::new);
    }

    public void update(Long id, Employee employee) {
        Employee toBeUpdatedEmployee = findById(id);
        if (employee.getSalary() != null) {
            toBeUpdatedEmployee.setSalary(employee.getSalary());
        }
        if (employee.getAge() != null) {
            toBeUpdatedEmployee.setAge(employee.getAge());
        }
        jpaEmployeeRepository.save(toBeUpdatedEmployee);
    }

    public List<Employee> findAllByGender(String gender) {
        return getJpaEmployeeRepository().findAll()
                .stream()
                .filter(employee -> employee.getGender().equals(gender))
                .collect(Collectors.toList());
    }

    public Employee create(Employee employee) {
        return jpaEmployeeRepository.save(employee);
    }

    public List<Employee> findByPage(Integer page, Integer size) {
        return jpaEmployeeRepository.findAll(PageRequest.of(page - 1, size)).getContent();
    }

    public void delete(Long id) {
        jpaEmployeeRepository.deleteById(id);
    }
}
