package com.afs.restapi.service;

import com.afs.restapi.entity.Company;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.repository.InMemoryCompanyRepository;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.InMemoryEmployeeRepository;
import com.afs.restapi.repository.JPACompanyRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyService {

    private final InMemoryCompanyRepository inMemoryCompanyRepository;
    private final JPACompanyRepository jpaCompanyRepository;

    private final InMemoryEmployeeRepository inMemoryEmployeeRepository;

    public CompanyService(InMemoryCompanyRepository inMemoryCompanyRepository, JPACompanyRepository jpaCompanyRepository, InMemoryEmployeeRepository inMemoryEmployeeRepository) {
        this.inMemoryCompanyRepository = inMemoryCompanyRepository;
        this.jpaCompanyRepository = jpaCompanyRepository;
        this.inMemoryEmployeeRepository = inMemoryEmployeeRepository;
    }

    public InMemoryCompanyRepository getCompanyRepository() {
        return inMemoryCompanyRepository;
    }

    public InMemoryEmployeeRepository getEmployeeRepository() {
        return inMemoryEmployeeRepository;
    }

    public List<Company> findAll() {
        return getCompanyRepository().getCompanies();
    }

    public List<Company> findByPage(Integer page, Integer size) {
        return getCompanyRepository().findByPage(page, size);
    }

    public Company findById(Long id) {
        Company company = getCompanyRepository().findById(id).orElseThrow(CompanyNotFoundException::new);
        List<Employee> employees = getEmployeeRepository().findByCompanyId(company.getId());
        company.setEmployees(employees);
        return company;
    }

    public void update(Long id, Company company) {
        Company toBeUpdatedCompany = findById(id);
        company.setName(company.getName());
        jpaCompanyRepository.save(toBeUpdatedCompany);
    }

    public Company create(Company company) {
        return jpaCompanyRepository.save(company);
    }

    public List<Employee> findEmployeesByCompanyId(Long id) {
        return jpaCompanyRepository.findById(id).map(company -> company.getEmployees()).orElseThrow(CompanyNotFoundException::new);
    }

    public void delete(Long id) {
        jpaCompanyRepository.deleteById(id);
    }
}
